
package com.ubs.core.repository.login

import com.ubs.domain.model.LoginInfo
import com.ubs.domain.model.Result
import com.ubs.domain.model.Success
import com.ubs.domain.repository.LoginRepository

public class LoginRepositoryImpl(var login: LoginInfo) : LoginRepository{
    override suspend fun getLoginDetail(login: LoginInfo): Result<Boolean> {
        return Success(true)
    }
}