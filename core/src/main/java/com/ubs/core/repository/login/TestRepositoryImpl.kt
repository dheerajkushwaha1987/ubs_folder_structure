
package com.ubs.core.repository.login

import com.ubs.core.cloud.api.ApiInterface
import com.ubs.core.cloud.api.RestClient
import com.ubs.domain.repository.TestRepository

public class TestRepositoryImpl() : TestRepository{

    private val apiInterface: ApiInterface by lazy {RestClient().get()!!}

    override suspend fun getLoginDetail() = try {
           apiInterface.fetchFeeds()
        }catch (e: Exception) {
            null
        }
        /*suspend fun checkVersion() = try {
            apiInterface.fetchFeeds()
        }catch (e: Exception) {
            null
        }*/
    }

