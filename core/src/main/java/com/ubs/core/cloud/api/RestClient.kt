package com.ubs.core.cloud.api


import com.ubs.core.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.GeneralSecurityException
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class RestClient {
    private var apiInterface: ApiInterface? = null

    init {
        apiInterface = setupRestClient()
    }

    fun get(): ApiInterface? = apiInterface

    private fun setupRestClient(): ApiInterface? {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(SelfSigningClientBuilder().createClient())
            .build()
        return retrofit.create(ApiInterface::class.java)
    }

    class SelfSigningClientBuilder {

        private fun configureClient(client: OkHttpClient.Builder): OkHttpClient {
            val certs =
                arrayOf<TrustManager>(object : X509TrustManager {
                    override fun getAcceptedIssuers(): Array<X509Certificate?> {
                        return arrayOfNulls(0)
                    }

                    @Throws(CertificateException::class)
                    override fun checkServerTrusted(
                        chain: Array<X509Certificate>,
                        authType: String
                    ) {
                    }

                    @Throws(CertificateException::class)
                    override fun checkClientTrusted(
                        chain: Array<X509Certificate>,
                        authType: String
                    ) {
                    }
                })
            var ctx: SSLContext? = null
            try {
                ctx = SSLContext.getInstance("TLS")
                ctx.init(null, certs, SecureRandom())
            } catch (ex: GeneralSecurityException) {
            }
            try {
                val hostnameVerifier =
                    HostnameVerifier { hostname, session -> true }
                client.hostnameVerifier(hostnameVerifier)
                client.sslSocketFactory(
                    ctx!!.socketFactory,
                    certs[0] as X509TrustManager
                )
            } catch (e: Exception) {
            }
            return client.build()
        }

        fun createClient(): OkHttpClient {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
            else HttpLoggingInterceptor.Level.NONE
            val client = OkHttpClient.Builder()
            client.apply {
                addInterceptor(httpLoggingInterceptor)
                networkInterceptors().add(MyNetworkInterceptor())
                connectTimeout(10, TimeUnit.SECONDS)
                readTimeout(30, TimeUnit.SECONDS)
                retryOnConnectionFailure(true)
            }
            return configureClient(client)
        }
    }

    class MyNetworkInterceptor : Interceptor {

        override fun intercept(chain: Interceptor.Chain): Response {
            val newRequest = chain.request().newBuilder()
            newRequest.addHeader("Authorization", "")
            return chain.proceed(newRequest.build())
        }

    }
}