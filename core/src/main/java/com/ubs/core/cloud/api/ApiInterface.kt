package com.ubs.core.cloud.api

import com.example.assignment.repository.model.FeedData
import retrofit2.Response
import retrofit2.http.GET

interface ApiInterface {

    @GET(".")
    suspend fun fetchFeeds() : Response<FeedData>
}