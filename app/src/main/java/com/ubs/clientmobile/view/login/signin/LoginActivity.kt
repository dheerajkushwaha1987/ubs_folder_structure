package com.ubs.clientmodule.ui.login.signin

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import com.ubs.clientmobile.R

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        var mainViewModel = ViewModelProviders.of(this).get(LoginViewmodel::class.java)
        mainViewModel.getLoginDetail()

        mainViewModel.checkVersion().observe(this,{
            it?.let {
                Log.i(">>>>>>>>>>","response"+(it))
            }

        })


    }
}