package com.ubs.clientmodule.ui.login.signin

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.ubs.core.repository.login.LoginRepositoryImpl
import com.ubs.core.repository.login.TestRepositoryImpl
import com.ubs.domain.model.LoginInfo
import com.ubs.domain.usecase.login.GetLoginUseCaseImpl
import com.ubs.domain.usecase.login.GetTestUseCaseImpl
import kotlinx.coroutines.*


class LoginViewmodel : ViewModel() {


      fun getLoginDetail(){
          
       val getLoginUseCaseImpl = GetLoginUseCaseImpl(LoginRepositoryImpl(LoginInfo("","")))
        GlobalScope.async {
            val result=getLoginUseCaseImpl.invoke(LoginInfo("",""))
            println(">>>login result="+result)
        }
    }

    fun checkVersion() = liveData(Dispatchers.IO) {
        val testlogin= GetTestUseCaseImpl(TestRepositoryImpl())
        val response = testlogin.invoke()
        if(response != null) {
            if(response.isSuccessful) emit(response.body()) else emit(null)
        }else emit(null)
    }

}