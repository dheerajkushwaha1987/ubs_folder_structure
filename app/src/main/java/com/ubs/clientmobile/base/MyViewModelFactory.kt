package com.ubs.clientmodule.base

import androidx.core.util.Supplier
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ubs.clientmodule.ui.login.signin.LoginViewmodel

class ViewModelProviderFactory<T : ViewModel?>(
    private val viewModelClass: Class<T>,
    private val viewModelSupplier: Supplier<T>
) :
    ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(viewModelClass)) {
            viewModelSupplier.get() as T
        } else {
            throw IllegalArgumentException("Unknown Class name " + viewModelClass.name)
        }
    }

}

inline fun <reified T : ViewModel> provideViewModel(fragmentActivity: FragmentActivity, supplier: Supplier<T>): T {

    val factory: ViewModelProviderFactory<T> = ViewModelProviderFactory(
        T::class.java, supplier
    )
    return ViewModelProvider(fragmentActivity, factory).get(T::class.java)
}

