package com.ubs.domain.usecase.login

import com.ubs.domain.base.BaseUseCase
import com.ubs.domain.model.LoginInfo
import com.ubs.domain.model.Result

interface GetLoginUseCase :BaseUseCase<LoginInfo,Boolean> {

    override suspend operator fun invoke(param: LoginInfo): Result<Boolean>
}