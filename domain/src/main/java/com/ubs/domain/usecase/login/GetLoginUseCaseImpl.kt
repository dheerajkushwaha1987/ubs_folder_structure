package com.ubs.domain.usecase.login

import com.ubs.domain.model.LoginInfo
import com.ubs.domain.repository.LoginRepository

class GetLoginUseCaseImpl(private val loginRepository: LoginRepository) : GetLoginUseCase{
    override suspend operator fun invoke(param: LoginInfo) =
         loginRepository.getLoginDetail(param)

}