package com.ubs.domain.usecase.login

import com.example.assignment.repository.model.FeedData
import com.ubs.domain.base.BaseUseCase
import com.ubs.domain.model.LoginInfo
import com.ubs.domain.model.Result
import retrofit2.Response

interface GetTestUseCase {
    suspend operator fun invoke(): Response<FeedData>?
}