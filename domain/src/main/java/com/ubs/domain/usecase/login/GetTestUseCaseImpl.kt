package com.ubs.domain.usecase.login

import com.example.assignment.repository.model.FeedData
import com.ubs.domain.repository.TestRepository
import retrofit2.Response

class GetTestUseCaseImpl(private val testRepository: TestRepository) : GetTestUseCase{

    override suspend fun invoke(): Response<FeedData>? = testRepository.getLoginDetail()

}