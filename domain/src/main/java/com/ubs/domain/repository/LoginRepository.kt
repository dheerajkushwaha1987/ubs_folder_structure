package com.ubs.domain.repository

import com.ubs.domain.model.LoginInfo
import com.ubs.domain.model.Result

interface LoginRepository {
   suspend fun getLoginDetail(login: LoginInfo): Result<Boolean>
}