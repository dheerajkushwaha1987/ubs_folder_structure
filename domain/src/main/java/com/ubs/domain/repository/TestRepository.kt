package com.ubs.domain.repository

import com.example.assignment.repository.model.FeedData
import retrofit2.Response


interface TestRepository {
   suspend fun getLoginDetail(): Response<FeedData>?
}