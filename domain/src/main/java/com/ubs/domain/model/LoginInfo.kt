package com.ubs.domain.model

data class LoginInfo(val username: String, val password: String)